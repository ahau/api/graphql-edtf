const { Kind } = require('graphql/language')
const EdtfDate = require('../src/resolvers/edtf-date')
const test = require('tape')
const TestBot = require('./test-bot')
const gql = require('graphql-tag')

const edtf = require('edtf')

test('edtf date serialize', async t => {
  const { server, apollo } = await TestBot()
  t.plan(20)

  // TESTS WITH APOLLO

  const QUERY = (input) => {
    return {
      query: gql`
        query {
          ${input}         
        }
      `
    }
  }

  // ----- VALID INPUTS

  const validString = await apollo.query(QUERY('dateOfBirthValidString'))
  t.deepEquals(
    validString.data.dateOfBirthValidString,
    '1985-04-12',
    'valid string returns string'
  )

  const validEdtf = await apollo.query(QUERY('dateOfBirthValidEdtfObject'))
  t.deepEquals(
    validEdtf.data.dateOfBirthValidEdtfObject,
    '1985-04-12',
    'valid edtf object returns string'
  )

  const validDate = await apollo.query(QUERY('dateOfBirthValidDateObject'))
  t.deepEquals(
    validDate.data.dateOfBirthValidDateObject,
    '1985-04-12T00:00:00.000Z',
    'valid date object returns string'
  )

  const validNumber = await apollo.query(QUERY('dateOfBirthValidNumber'))
  t.deepEquals(
    validNumber.data.dateOfBirthValidNumber,
    '1985-04-12T00:00:00.000Z',
    'valid number returns string'
  )

  const nullValue = await apollo.query(QUERY('dateNull'))
  t.deepEquals(
    nullValue.data.dateNull,
    null,
    'null value returns null'
  )

  // ----- INVALID INPUTS

  await apollo.query(QUERY('invalidString'))
    .catch(err => t.match(err.message, /Invalid EDTF date/))

  await apollo.query(QUERY('invalidObject'))
    .catch(err => {
      t.deepEquals(
        err.message,
        'Expected a value of type Edtf Date but received: {"date":"this is an invalid date"} instead.'
      )
    })

  await apollo.query(QUERY('invalidNumber'))
    .catch(err => {
      t.deepEquals(
        err.message,
        'Expected value of type number to be a valid date in milliseconds. Got -1.2318231237123817e+23 instead.'
      )
    })

  await apollo.query(QUERY('invalidEmptyString'))
    .catch(err => {
      t.deepEquals(
        err.message,
        'Invalid EDTF date ""',
        'invalid empty string returns error'
      )
    })

  await apollo.query(QUERY('invalidBoolean'))
    .catch(err => {
      t.deepEquals(
        err.message,
        'Value is not an instance of date or of type string, object, number or null. Got false instead',
        'invalid boolean returns error'
      )
    })

  // TESTS DIRECTLY ON THE FUNCTIONS

  // ----- VALID INPUTS

  t.doesNotThrow(
    () => EdtfDate.serialize('0102-03-04'),
    'valid date string doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.serialize(edtf('0102-03-04')),
    'valid edtf object doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.serialize(new Date('0102-03-04')),
    'valid date object doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.serialize(12345678),
    'valid date number doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.serialize(null),
    'null value doesnt throw error'
  )

  // ----- INVALID INPUTS

  t.throws(
    () => EdtfDate.serialize('not a valid date'),
    'invalid string throws error'
  )

  t.throws(
    () => EdtfDate.serialize({ date: 'not a valid date' }),
    'invalid object throws error'
  )

  t.throws(
    () => EdtfDate.serialize(-1231823123712381),
    'invalid number throws error'
  )

  t.throws(
    () => EdtfDate.serialize(''),
    'invalid empty string throws error'
  )

  t.throws(
    () => EdtfDate.serialize(false),
    'invalid boolean throws error'
  )

  server.close()
})

test('edtf date parseValue', async t => {
  const { apollo, server } = await TestBot()
  t.plan(25)

  // TESTS WITH APOLLO

  const MUTATION = input => ({
    mutation: gql`
      mutation($input: EdtfDate) {
        saveDate(input: $input)
      }
    `,
    variables: { input }
  })

  // ----- VALID INPUTS

  const validString = await apollo.mutate(MUTATION('1234-04-12'))
  t.notOk(validString.errors, 'valid string has no errors')
  t.deepEquals(validString.data.saveDate, '1234-04-12')

  const validEdtfObject = await apollo.mutate(MUTATION(edtf('1234-04-12')))
  t.notOk(validEdtfObject.errors, 'valid edtf object has no errors')
  t.deepEquals(validEdtfObject.data.saveDate, '1234-04-12', 'valid edtf object returns correctly')

  const validDateObject = await apollo.mutate(MUTATION(new Date('1234-04-12')))
  t.notOk(validDateObject.errors, 'valid date object has no errors')
  t.deepEquals(validDateObject.data.saveDate, '1234-04-12T00:00:00.000Z', 'valid date object returns correctly')

  const validNumber = await apollo.mutate(MUTATION(-23217145140000))
  t.notOk(validNumber.errors, 'valid number has no errors')
  t.deepEquals(validNumber.data.saveDate, '1234-04-12T01:01:00.000Z', 'valid number returns correctly')

  const validNull = await apollo.mutate(MUTATION(null))
  t.notOk(validNull.errors, 'valid null has no errors')
  t.deepEquals(validNull.data.saveDate, null, 'valid null returns correctly')

  // ----- INVALID INPUTS

  await apollo.mutate(MUTATION('this is an invalid date'))
    .catch(err => t.ok(err, 'invalid string has errors'))

  await apollo.mutate(MUTATION({ date: 'this is an invalid date' }))
    .catch(err => t.ok(err, 'invalid object args has errors'))

  await apollo.mutate(MUTATION(-123182312371233))
    .catch(err => t.ok(err, 'invalid number args has errors'))

  await apollo.mutate(MUTATION(''))
    .catch(err => t.ok(err, 'invalid empty string args has errors'))

  await apollo.mutate(MUTATION(false))
    .catch(err => t.ok(err, 'invalid boolean args has errors'))

  // TESTS DIRECTLY ON THE FUNCTIONS

  t.doesNotThrow(
    () => EdtfDate.parseValue('0102-03-04'),
    'valid date string doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.parseValue(edtf('0102-03-04')),
    'valid edtf object doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.parseValue(new Date('0102-03-04')),
    'valid date object doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.parseValue(12345678),
    'valid date number doesnt throw error'
  )

  t.doesNotThrow(
    () => EdtfDate.parseValue(null),
    'null value doesnt throw error'
  )

  // ----- INVALID INPUTS

  t.throws(
    () => EdtfDate.parseValue('not a valid date'),
    'invalid string throws error'
  )

  t.throws(
    () => EdtfDate.parseValue({ date: 'not a valid date' }),
    'invalid object throws error'
  )

  t.throws(
    () => EdtfDate.parseValue(-12318231237123813), // eslint-disable-line
    'invalid number throws error'
  )

  t.throws(
    () => EdtfDate.parseValue(''),
    'invalid empty string throws error'
  )

  t.throws(
    () => EdtfDate.parseValue(false),
    'invalid boolean throws error'
  )

  server.close()
})

test('edtf date parseLiteral', async t => {
  const { apollo, server } = await TestBot()
  t.plan(20)

  // TESTS WITH APOLLO

  const MUTATION = input => ({
    mutation: gql`
      mutation {
        saveDate(input: "${input}")
      }
    `
  })

  // ----- VALID INPUTS

  const validString = await apollo.mutate(MUTATION('1234-04-12'))
  t.notOk(validString.errors, 'valid string has no errors')
  t.deepEquals(validString.data.saveDate, '1234-04-12', 'valid string returns correctly')

  const validNull = await apollo.mutate(MUTATION(null))
  t.notOk(validNull.errors, 'valid null has no errors')
  t.deepEquals(validNull.data.saveDate, null, 'valid null returns correctly')

  // NOTE: accepts edtf object because it is converted to a string by graphql
  const validEdtfObject = await apollo.mutate(MUTATION(edtf('1995-07-24')))
  t.notOk(validEdtfObject.errors, 'edtf object args has no errors')
  t.deepEquals(validEdtfObject.data.saveDate, '1995-07-24', 'valid edtf object returns correctly')

  // ----- INVALID INPUTS

  await apollo.mutate(MUTATION('this is an invalid date'))
    .catch(err => t.ok(err, 'invalid string has errors'))

  await apollo.mutate(MUTATION(-12318231237122))
    .catch(err => t.ok(err, 'invalid number args has errors'))

  await apollo.mutate(MUTATION(''))
    .catch(err => t.ok(err, 'invalid empty string args has errors'))

  await apollo.mutate(MUTATION(false))
    .catch(err => t.ok(err, 'invalid boolean args has errors'))

  await apollo.mutate(MUTATION(new Date('1995-07-24')))
    .catch(err => t.ok(err, 'invalid Date object args has errors'))

  // TESTS DIRECTLY ON THE FUNCTIONS

  t.doesNotThrow(
    () => EdtfDate.parseLiteral({ value: '1985-04-12', kind: Kind.STRING }),
    'valid date string doesnt throw error'
  )

  // NOTE: accepts edtf object because it is converted to a string
  t.doesNotThrow(
    () => EdtfDate.parseLiteral({ value: edtf('0102-03-04'), kind: Kind.STRING }),
    'valid edtf object doesnt throw error'
  )

  // NOTE: null is converted to a string
  t.doesNotThrow(
    () => EdtfDate.parseLiteral({ value: 'null', kind: Kind.STRING }),
    'null value doesnt throw error'
  )

  // ----- INVALID INPUTS

  t.throws(
    () => EdtfDate.parseLiteral('not a valid date'),
    'invalid string throws error'
  )

  t.throws(
    () => EdtfDate.parseLiteral({ date: 'not a valid date' }),
    'invalid object throws error'
  )

  // NOTE: cant accept date objects
  t.throws(
    () => EdtfDate.parseLiteral({ value: new Date('0102-03-04').toString(), kind: Kind.STRING }),
    'invalid date object throws error'
  )

  t.throws(
    () => EdtfDate.parseLiteral(-123182312371238172397123),
    'invalid number throws error'
  )

  t.throws(
    () => EdtfDate.parseLiteral(''),
    'invalid empty string throws error'
  )

  t.throws(
    () => EdtfDate.parseLiteral(false),
    'invalid boolean throws error'
  )

  server.close()
})
