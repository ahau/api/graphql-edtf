const { Kind } = require('graphql/language')
const EdtfInterval = require('../src/resolvers/edtf-interval')
const test = require('tape')
const TestBot = require('./test-bot')
const gql = require('graphql-tag')
const edtf = require('edtf')

test('edtf interval serialize', async t => {
  const { apollo, server } = await TestBot()
  t.plan(14)

  const QUERY = (input) => {
    return {
      query: gql`
        query {
          ${input}
        }
      `
    }
  }

  // VALID INPUTS

  const validIntervalString = await apollo.query(QUERY('lifespanValidString'))
  t.deepEquals(
    validIntervalString.data.lifespanValidString,
    '1985-04-12/1990-06-12',
    'valid string returns string'
  )

  const validIntervalObject = await apollo.query(QUERY('lifespanValidObject'))
  t.deepEquals(
    validIntervalObject.data.lifespanValidObject,
    '1985-04-12/1990-06-12',
    'valid object returns string'
  )

  const intervalNull = await apollo.query(QUERY('intervalNull'))
  t.deepEquals(
    intervalNull.data.intervalNull,
    null,
    'null interval returns null'
  )

  // INVALID INPUTS

  await apollo.query(QUERY('invalidIntervalString'))
    .catch(err => t.ok(err, 'invalid string returns error'))

  await apollo.query(QUERY('invalidIntervalObject'))
    .catch(err => t.deepEquals(
      err.message,
      'Expected a value of type Edtf Interval but received: {"date":"this is an invalid interval"} instead.',
      'invalid object returns error'
    ))

  await apollo.query(QUERY('invalidIntervalNumber'))
    .catch(err => t.deepEquals(
      err.message,
      'Invalid EDTF date "2361-03-08T22:34:14.321Z"',
      'invalid number input returns error'
    ))

  // TESTS DIRECTLY ON THE FUNCTIONS

  // VALID INPUTS
  t.doesNotThrow(
    () => EdtfInterval.serialize('1995-07-24/2020-06-02'),
    'valid string doesnt throw'
  )

  t.doesNotThrow(
    () => EdtfInterval.serialize(edtf('1995-07-24/2020-06-02')),
    'valid edtf object doesnt throw'
  )

  t.doesNotThrow(
    () => EdtfInterval.serialize(null),
    'null input doesnt throw'
  )

  // INVALID INPUTS

  t.throws(
    () => EdtfInterval.serialize('not a valid interval'),
    'invalid date string throws error'
  )

  t.throws(
    () => EdtfInterval.serialize({ date: 'not a valid interval' }),
    'invalid date object throws error'
  )

  t.throws(
    () => EdtfInterval.serialize(-123182312371238172397123),
    'invalid date number throws error'
  )

  t.throws(
    () => EdtfInterval.serialize(''),
    'empty string throws error'
  )

  t.throws(
    () => EdtfInterval.serialize(false),
    'boolean throws error'
  )

  server.close()
})

test('edtf interval parseValue', async t => {
  const { apollo, server } = await TestBot()
  t.plan(21)
  const MUTATION = input => ({
    mutation: gql`
      mutation($input: EdtfInterval) {
        saveInterval(input: $input)
      }
    `,
    variables: { input }
  })

  // VALID INPUTS

  const validIntervalString = await apollo.mutate(MUTATION('1995-07-24/2020-06-03'))
  t.notOk(validIntervalString.errors, 'valid interval string has no errors')
  t.deepEquals(validIntervalString.data.saveInterval, '1995-07-24/2020-06-03', 'valid string returns string')

  const validIntervalObject = await apollo.mutate(MUTATION(edtf('1995-07-24/2020-06-03')))
  t.notOk(validIntervalObject.errors, 'valid interval object has no errors')
  t.deepEquals(validIntervalObject.data.saveInterval, '1995-07-24/2020-06-03', 'edtf interval object returns string')

  const validIntervalNull = await apollo.mutate(MUTATION(null))
  t.notOk(validIntervalNull.errors, 'null interval has no errors')
  t.deepEquals(validIntervalNull.data.saveInterval, null, 'null returns null')

  // INVALID INPUTS

  await apollo.mutate(MUTATION('this is an invalid interval'))
    .catch(err => t.ok(err, 'invalid string has errors'))

  await apollo.mutate(MUTATION({ date: 'this is an invalid date' }))
    .catch(err => t.ok(err, 'invalid object args has errors'))

  await apollo.mutate(MUTATION(new Date('1234-02-01')))
    .catch(err => t.ok(err, 'invalid date object args has errors'))

  await apollo.mutate(MUTATION(482112000000))
    .catch(err => t.ok(err, 'invalid number args has errors'))

  await apollo.mutate(MUTATION(false))
    .catch(err => t.ok(err, 'invalid boolean args has errors'))

  await apollo.mutate(MUTATION(''))
    .catch(err => t.ok(err, 'invalid empty string args has errors'))

  // TESTS DIRECTLY ON THE FUNCTIONS

  // VALID INPUTS

  t.doesNotThrow(() => EdtfInterval.parseValue('1985-04-12/1995-04-12'), 'parses valid interval string')
  t.doesNotThrow(() => EdtfInterval.parseValue(edtf('1985-04-12/1995-04-12')), 'parses valid interval edtf object')

  // INVALID INPUTS

  t.throws(
    () => EdtfInterval.parseValue('1985-04-12'),
    'There was an error parsing the value as an Edtf Interval of input type string. Got "1985-04-12"',
    'string must be an interval'
  )

  t.throws(() => EdtfInterval.parseValue('not a valid interval'), 'throws error on invalid string')
  t.throws(() => EdtfInterval.parseValue('195-04-12'), 'throws error on invalid interval string')

  t.throws(() => EdtfInterval.parseValue(123123123), 'throws error on number input')
  t.throws(() => EdtfInterval.parseValue({ date: 'not a valid date' }), 'throws error on object input')
  t.throws(() => EdtfInterval.parseValue(false), 'throws error on boolean input')
  t.throws(() => EdtfInterval.parseValue(''), 'throws error on empty string input')

  server.close()
})

test('edtf date parseLiteral', async t => {
  const { apollo, server } = await TestBot()
  t.plan(18)

  const MUTATION = input => ({
    mutation: gql`
      mutation {
        saveInterval(input: "${input}")
      }
    `
  })

  // VALID INPUTS

  const validInterval = await apollo.mutate(MUTATION('1234-04-12/1234-05-23'))
  t.deepEquals(
    validInterval.data.saveInterval,
    '1234-04-12/1234-05-23',
    'valid interval returns string'
  )

  const validInterval2 = await apollo.mutate(MUTATION('XXXX-04-12/1234-XX-23'))
  t.deepEquals(
    validInterval2.data.saveInterval,
    'XXXX-04-12/1234-XX-23',
    'valid unspecified interval returns string'
  )

  const validObject = await apollo.mutate(MUTATION(edtf('XXXX-04-12/1234-XX-23')))
  t.deepEquals(
    validObject.data.saveInterval,
    'XXXX-04-12/1234-XX-23',
    'valid interval object returns string'
  )

  const validNull = await apollo.mutate(MUTATION(null))
  t.deepEquals(
    validNull.data.saveInterval,
    null,
    'null returns null'
  )

  // INVALID INPUTS

  await apollo.mutate(MUTATION('this is an invalid interval'))
    .catch(err => t.ok(err, 'invalid string has errors'))

  await apollo.mutate(MUTATION({ date: 'this is an invalid date' }))
    .catch(err => t.ok(err, 'invalid object args has errors'))

  await apollo.mutate(MUTATION(482112000000))
    .catch(err => t.ok(err, 'invalid number args has errors'))

  await apollo.mutate(MUTATION(false))
    .catch(err => t.ok(err, 'invalid boolean args has errors'))

  await apollo.mutate(MUTATION(''))
    .catch(err => t.ok(err, 'invalid empty string args has errors'))

  // TESTS DIRECTLY ON THE FUNCTIONS

  // VALID INPUTS

  t.deepEquals(EdtfInterval.parseLiteral({ value: '1985-04-12/', kind: Kind.STRING }), '1985-04-12/', 'returns interval')
  t.deepEquals(EdtfInterval.parseLiteral({ value: '1985-XX-12/2000-XX-25', kind: Kind.STRING }), '1985-XX-12/2000-XX-25', 'returns X interval')
  t.deepEquals(EdtfInterval.parseLiteral({ value: '1985-04-12?/1990?', kind: Kind.STRING }), '1985-04-12?/1990?', 'returns ? interval')
  t.deepEquals(EdtfInterval.parseLiteral({ value: edtf('1995-07-24/2020-06-02'), kind: Kind.STRING }), '1995-07-24/2020-06-02', 'edtf object returns string')
  t.deepEquals(EdtfInterval.parseLiteral({ value: 'null', kind: Kind.STRING }), null, 'null returns null')

  // INVALID INPUTS

  t.throws(
    () => EdtfInterval.parseLiteral({ value: 'this is an invalid interval', kind: Kind.STRING }),
    'throws error on invalid string'
  )

  t.throws(
    () => EdtfInterval.parseLiteral({ value: '1955-04-12', kind: Kind.STRING }),
    'throws error on invalid interval string'
  )

  t.throws(
    () => EdtfInterval.parseLiteral({ value: 123123123, kind: Kind.INT }),
    'throws error on number input'
  )

  t.throws(
    () => EdtfInterval.parseLiteral({ value: { date: 'not a valid date' }, kind: Kind.OBJECT }),
    'throws error on object input'
  )

  server.close()
})
