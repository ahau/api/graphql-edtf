const GraphQLDate = require('graphql-date')
const EdtfDate = require('./src/resolvers/edtf-date')
const EdtfInterval = require('./src/resolvers/edtf-interval')

module.exports = {
  Date: GraphQLDate,
  EdtfDate,
  EdtfInterval
}
