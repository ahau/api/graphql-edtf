# graphql-edtf

Graphql Edtf Date Type

## Installation

```
npm i --save graphql-edtf
```

## Usage

Resolvers
```js
const { Date, EdtfDate, EdtfInterval } = require('graphql-edtf')

const resolvers = {
  Date,
  EdtfDate,
  EdtfInterval
}

```

TypeDefs
```js
const { gql } = require('apollo-server')

module.exports = gql`
  # Define scalar types for each
  scalar Date
  scalar EdtfDate
  scalar EdtfInterval

  type Person {
    id: ID!
    name: String
    aliveInterval: EdtfInterval
  }

  type Record {
    id: ID!
    name: String
    recordStartDate: EdtfDate # allows for fuzzy dates e.g. 1995? (i think the year is 1995)
  }

  type Tombstone {
    date: Date # graphql-date
  }

  type Query {
    person(id: String!): Person
    record(id: String!): Record
  }
`
```
