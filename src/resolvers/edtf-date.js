const { Kind, GraphQLScalarType } = require('graphql')
const parse = require('../lib/parse')
const toEdtfString = require('../lib/to-edtf-string')

const type = 'Date'

module.exports = new GraphQLScalarType({
  name: 'EdtfDate',

  description: 'Use JavaScript EdtfDate for date fields.',

  serialize (value) {
    if (value === null) return null
    let v = value
    parse(v, type)
    v = toEdtfString(v)
    return v
  },

  parseValue (value) {
    if (value === null) return null
    parse(value, type)
    return toEdtfString(value)
  },

  parseLiteral (ast) {
    let v = ast.value

    if (ast.kind !== Kind.STRING) {
      throw new Error(`Expected value to be of type string. Got ${ast.kind} instead.`)
    }

    parse(v, type)
    v = toEdtfString(v)
    return v
  }
})
