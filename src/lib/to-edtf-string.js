const edtf = require('edtf')

module.exports = function toEdtfString (value) {
  switch (typeof value) {
    case 'string':
      if (value === 'null') return null
      return edtf(value).edtf
    case 'object':
      return edtf(value).edtf
    case 'number':
      var date = new Date(value)
      return edtf(date).edtf
    default:
      throw new Error('Something went wrong with converting the given value to an edtf string')
  }
}
